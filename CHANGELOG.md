# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.1.1 (2021-05-22)


### Others

* **project:** add commit linting and automatic changelog generation ([212d336](https://gitlab.com/uulmhack/boilerplates/react-typescript-application-starterkit/commit/212d3367207cc8cc1f4a2ebf87f266f53d1b6784))

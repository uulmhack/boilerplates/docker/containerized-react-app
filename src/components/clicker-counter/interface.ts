export interface IClickCounterProps {
    title?: string
    initialCounterValue: number
}

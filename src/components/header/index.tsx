import React, { FC } from "react"

interface IHeaderProps {
    heading: string
}
export const Header: FC<IHeaderProps> = (headerProps: IHeaderProps) => {
    const {
        heading
    } = headerProps
    return (
        <>
            <h1> { heading } </h1>
        </>
    )
}
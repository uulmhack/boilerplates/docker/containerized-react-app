import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Header } from "./components/header"
import { ClickCounter } from './components/clicker-counter';

export const App = () => {

  return (
    <div className="App">
      <Header heading="Hello React World"/>
      <div>
        <span>I'm the content of a span which is nested in a div</span>
        <ClickCounter initialCounterValue={ 0 }/>
        <ClickCounter initialCounterValue={ 0 } title="uulmhack demo title"/>
      </div>
    </div>
  );
}
FROM node:14.7-alpine3.12

WORKDIR /app

ADD . /app

RUN yarn install

CMD ["yarn", "start"]
